package com.mydailymistakes.timefighter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivityTAG"

    // declaring ui elements
    private lateinit var tapMeButton: Button
    private lateinit var gameScoreTextView: TextView
    private lateinit var timeLeftTextView: TextView
    private lateinit var resetButton: Button

    // property that will store the scores
    private var score = 0
    private var gameStarted = false
    private lateinit var countDownTimer: CountDownTimer
    private val initialCountDown: Long = 20000
    private var timeLeftInSeconds : Long = initialCountDown / 1000
    private val countDownInterval: Long = 1000

    // state storage keys
    private val KEY_GAME_STARTED = "com.mydailymistakes.timefighter.MainActivity.KEY_GAME_STARTED"
    private val KEY_SCORE = "com.mydailymistakes.timefighter.MainActivity.KEY_SCORE"
    private val KEY_TIME_LEFT = "com.mydailymistakes.timefighter.MainActivity.KEY_TIME_LEFT"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // binding ui element objects with the corresponding xml id for that ui
        tapMeButton = findViewById(R.id.tap_me_button)
        gameScoreTextView = findViewById(R.id.score_text_view)
        timeLeftTextView = findViewById(R.id.time_left_text_view)
        resetButton = findViewById(R.id.reset_button)

        setupGame()

        // setting a function to run when user clicks the button.
        tapMeButtonClick()
        resetButtonClick()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putBoolean(KEY_GAME_STARTED, gameStarted)
        outState?.putInt(KEY_SCORE, score)
        outState?.putLong(KEY_TIME_LEFT, timeLeftInSeconds)
        countDownTimer.cancel()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        gameStarted = savedInstanceState?.getBoolean(KEY_GAME_STARTED) ?: false
        score = savedInstanceState?.getInt(KEY_SCORE) ?: 0
        timeLeftInSeconds = savedInstanceState?.getLong(KEY_TIME_LEFT) ?: 0

        gameScoreTextView.text = getString(R.string.your_score, score.toString())
        timeLeftTextView.text = getString(R.string.time_left, timeLeftInSeconds.toString())

        if(gameStarted) {
            startCountDownTimer(timeLeftInSeconds * 1000, countDownInterval)
            countDownTimer.start()
        }
    }

    fun resetButtonClick() {
        resetButton.setOnClickListener {
            tapMeButton.isEnabled = true
            resetButton.visibility = View.INVISIBLE
            setupGame()
        }
    }

    fun tapMeButtonClick() {
        tapMeButton.setOnClickListener {
            if (!gameStarted) {
                startGame()
            }

            incrementScore()
        }
    }

    private fun startGame() {
        gameStarted = true
        countDownTimer.start()
    }

    private fun incrementScore() {
        score++
        updateScoreTextView()
    }

    private fun setupGame() {
        gameStarted = false
        score = 0
        updateScoreTextView()
        resetTimerTextView()

        startCountDownTimer(initialCountDown, countDownInterval)
    }

    fun startCountDownTimer(initialCountDown: Long, countDownInterval: Long) {
        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onFinish() {
                Log.d(TAG, "Timer Finished")

                endGame()
            }

            override fun onTick(milliSecondsUntilFinished: Long) {
                Log.d(TAG, "Time Left: $milliSecondsUntilFinished")
                timeLeftInSeconds = milliSecondsUntilFinished / 1000
                timeLeftTextView.text = getString(R.string.time_left, timeLeftInSeconds.toString())
            }
        }
    }

    private fun resetTimerTextView() {
        val initialTimeLeft = initialCountDown / 1000
        timeLeftTextView.text = getString(R.string.time_left, initialTimeLeft.toString())
    }

    private fun updateScoreTextView() {
        val newScore = getString(R.string.your_score, score.toString())
        score_text_view.text = newScore
    }

    private fun endGame() {
        Toast.makeText(this, getString(R.string.game_over_message, score.toString()), Toast.LENGTH_LONG).show()
        tapMeButton.isEnabled = false
        resetButton.visibility = View.VISIBLE
        setupGame()
    }
}
